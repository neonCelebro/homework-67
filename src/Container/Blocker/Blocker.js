import React, {Component} from 'react';
import  {connect} from 'react-redux';
import './Blocker.css';

class Blocker extends Component{
  render(){
    return(
      <form>
        <input className={this.props.required}  disabled type={this.props.required === 'access'? 'text' : 'password'} value={this.props.required === 'access'? 'Access Granted':this.props.tryPass}/>
        <input onClick={(value)=> this.props.addKey('1')} type='button' value='1'/>
        <input onClick={(value)=> this.props.addKey('2')} type='button' value='2'/>
        <input onClick={(value)=> this.props.addKey('3')} type='button' value='3'/>
        <input onClick={(value)=> this.props.addKey('4')} type='button' value='4'/>
        <input onClick={(value)=> this.props.addKey('5')} type='button' value='5'/>
        <input onClick={(value)=> this.props.addKey('6')} type='button' value='6'/>
        <input onClick={(value)=> this.props.addKey('7')} type='button' value='7'/>
        <input onClick={(value)=> this.props.addKey('8')} type='button' value='8'/>
        <input onClick={(value)=> this.props.addKey('9')} type='button' value='9'/>
        <input value='<' type='button' onClick={this.props.remuveKey}/>
        <input onClick={(value)=> this.props.addKey('0')} type='button' value='0'/>
        <input value='E' type='button' onClick={this.props.enter}/>
      </form>
    );
  }
};
const mapStoretoProps = state =>{
  return {
    tryPass : state.tryPass,
    required: state.required,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addKey: (value) => dispatch({type: 'ADD_KEY', value}),
    enter: (e) => {e.preventDefault(); dispatch({type: 'CHEACK_PASS'})},
    remuveKey: (e) => {e.preventDefault(); dispatch({type: 'REMUVE_KEY'})}
  }
}

export default connect(mapStoretoProps, mapDispatchToProps)(Blocker);
