const initialState = {
  password : '1234',
  tryPass: '',
  required: '',
}

const reducer = (state = initialState, action) =>{
  if (action.type === 'ADD_KEY' && state.tryPass.length < 4) {
    return{
      ...state,
      tryPass: state.tryPass.concat(action.value)
    }
  }
  if (action.type === 'CHEACK_PASS') {
    if (state.tryPass === state.password) {
      return{
        ...state,
        required: 'access',
      }
    } else {
      return {
        ...state,
        required: 'error',
      }
    }
  }
  if (action.type === 'REMUVE_KEY') {
    return{
      ...state,
      tryPass: state.tryPass.slice(0, -1),
      required: '',
    }
  };
  return state;
};

export default reducer;
